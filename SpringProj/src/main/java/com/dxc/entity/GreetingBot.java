package com.dxc.entity;

import com.dxc.service.GreetingService;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Component;

@Component
public class GreetingBot {
	@Autowired
	final private GreetingService service;

	public GreetingBot(GreetingService service) {
		this.service = service;
	}
	
	public void print() {
		System.out.println(this.service.greet());
	}

}
