package com.dxc;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.*;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

import com.dxc.entity.Employee;
import com.dxc.entity.GreetingBot;
import com.dxc.entity.HelloWorld;
import com.dxc.service.GreetingService;


@Configuration
@ComponentScan
public class App 
{
    public static void main( String[] args )
    {
//    	//Lazy loading mechanism
//    	BeanFactory beanFactory = new XmlBeanFactory(new ClassPathResource("applicationContext.xml"));
//        Employee employee = (Employee) beanFactory.getBean("employeeBean");
//        System.out.println(employee);
        
    
//    	//Eager loading mechanism
//    	ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
//    	Employee employee = (Employee) applicationContext.getBean("employeeBean");
//    	System.out.println(employee);
    	
//    	ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
//    	HelloWorld objA = (HelloWorld) context.getBean("helloWorldBean");
//    	objA.setMessage("I am object A");
//    	System.out.println(objA.getMessage());
//    	HelloWorld objB = (HelloWorld) context.getBean("helloWorldBean");
//    	objB.setMessage("I am object B");
//    	System.out.println(objB.getMessage());
    	
	
		ApplicationContext context = new AnnotationConfigApplicationContext(App.class);
    	GreetingBot greeter = context.getBean(GreetingBot.class);
    	greeter.print();
    	
    	
    }
}

@Configuration
@ComponentScan
class BeanConfig{
	@Bean
	GreetingService mockGreetingService() {
		return new GreetingService() {
			public String greet() {
				return "Good Day!";
			}
		};
	}
}

